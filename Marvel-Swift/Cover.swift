//
//  Cover.swift
//  Marvel-Swift
//
//  Created by Manuela Ungureanu on 20/03/16.
//  Copyright © 2016 Manuela Ungureanu. All rights reserved.
//

import UIKit

public class Cover  {

    public var id:Int?
    public var thumbnail: String?
    public var title: String?
    public var modifield: NSDate?
    
    public init() {}
    

    init?(data:NSDictionary?)
    {
        
        id = data?.valueForKey("id") as? Int
        modifield = data?.valueForKey("modified") as? NSDate
        title = data?.valueForKey("title") as? String
        
        guard let dic = data?.valueForKey("thumbnail") as? NSDictionary,
              let path = dic.valueForKey("path") as? String,
              let ext = dic.valueForKey("extension") as? String
        else
        {
            thumbnail = nil;
            return;
        }
        thumbnail = "\(path).\(ext)"
        
    }
}
