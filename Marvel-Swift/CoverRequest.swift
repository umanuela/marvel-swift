//
//  CoverRequest.swift
//  Marvel-Swift
//
//  Created by Manuela Ungureanu on 20/03/16.
//  Copyright © 2016 Manuela Ungureanu. All rights reserved.
//

import UIKit

class CoverRequest {

    internal init() {}
    
    static let JSONExtension = ".json"
    static let MarvelCoversURL = "http://gateway.marvel.com/v1/public/comics?apikey=17e5cf7245ade93d52297b197aa8b519&hash=03b622b6cf45f9e593e440d5f240271e&ts=1458484320"
        
    
    internal func fetchCoversWithCompletionHandler(updateUIHandler: (covers: Array<Cover>?) -> Void){
    
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        let url = NSURL(string: CoverRequest.MarvelCoversURL)
        let request = NSURLRequest(URL: url!)
        let task = session.dataTaskWithRequest(request)
        {(let data, let response, let error) in
        
            guard let _:NSData = data, let _:NSURLResponse = response where error == nil else
            {
                debugPrint("marvel covers request error: \(error)")
                return
            }
            
            do {
                let dict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary
                let serializedData =  dict!.objectForKey("data") as? NSDictionary
                let covers = self.getCovers(serializedData?.objectForKey("results") as? NSArray)
                
                dispatch_async(dispatch_get_main_queue()){
                    updateUIHandler(covers: covers)
                }
            } catch let error {
                debugPrint("json error: \(error)")
                return
            }
        }
        task.resume()
    }
    
    private func getCovers(fromArray: NSArray?) -> Array<Cover>?{
        
        guard let _ = fromArray else{
            return nil;
        }
        
        var covers = Array<Cover>()
        for item in fromArray!{
            
            if let cover = Cover(data: item as? NSDictionary){
                covers.append(cover);
            }
        }
        return covers;
    }
    
    //TO DO: add params to the request to get the latest covers
    internal var requestForNewer: CoverRequest? {
        return CoverRequest()
    }
}
