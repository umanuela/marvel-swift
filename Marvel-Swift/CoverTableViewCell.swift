//
//  CoverTableViewCell.swift
//  Marvel-Swift
//
//  Created by Manuela Ungureanu on 20/03/16.
//  Copyright © 2016 Manuela Ungureanu. All rights reserved.
//

import UIKit

//internal protocol ChangeCoverProtocol {
//    func pickNewCover() -> Void
//}

class CoverTableViewCell: UITableViewCell {
    
    //MARK: Properties
    //var delegate: ChangeCoverProtocol?
    
    internal var cover: Cover? {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var coverImageView: UIImageView!

    var actionAfterDownload :((UIImage?) -> Void)?
    
    // convenience computed property, enable to run code every time an image is set in imageView, like resize the imageView
    internal var img: UIImage? {
        get { return coverImageView.image }
        set {
            coverImageView?.image = newValue
            coverImageView?.sizeThatFits(self.bounds.size)
        }
    }
    
    var imageURL: NSURL? {
        didSet {
            img = nil
            //check if the imageview is still displayed to do the fetch
            if coverImageView?.window != nil
            {
                fetchImage()
            }
        }
    }    
    
    //MARK: Functions
    private func updateUI() {
        
        guard let _ = cover, _ = cover?.thumbnail else{
            imageURL = nil;
            return;
        }
        imageURL = NSURL(string: cover!.thumbnail!)        
        
    }
    
    // fetches the image at imageURL and then display in UI on main thread
    private func fetchImage()
    {
        if let url = imageURL {
            
            let qos = Int(QOS_CLASS_USER_INITIATED.rawValue)
            dispatch_async(dispatch_get_global_queue(qos, 0)) { () -> Void in
                let imageData = NSData(contentsOfURL: url)
                dispatch_async(dispatch_get_main_queue()) {
                    [unowned self] () -> Void in
                    // only do something with this image if the url we fetched is the current imageURL we want
                    // (that might have changed while we were off fetching this one)
                    if url == self.imageURL
                    {
                        if imageData != nil {
                            self.img = UIImage(data: imageData!)
                        } else {
                            self.img = nil
                        }
                        //run a closure if set from table view controller
                        if let _ = self.actionAfterDownload{
                            self.actionAfterDownload!(self.img)
                        }
                    }
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
