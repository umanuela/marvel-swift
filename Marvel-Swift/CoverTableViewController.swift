//
//  MarvelTableViewController.swift
//  Marvel-Swift
//
//  Created by Manuela Ungureanu on 20/03/16.
//  Copyright © 2016 Manuela Ungureanu. All rights reserved.
//

import UIKit

public extension String {
    var NS: NSString { return (self as NSString) }
}

class CoverTableViewController: UITableViewController, UINavigationControllerDelegate,  UIImagePickerControllerDelegate, DBRestClientDelegate {

    //MARK: static constants
    static let PNGExtension = ".png"
    static let DropboxPath = "/"
    let localPath = NSTemporaryDirectory()
    
    // MARK: Properties
    @IBOutlet var coverTableView: UITableView!
    var imageCache = [String:UIImage]()
    
    var lastSuccessfullRequest: CoverRequest?
    
    var nextRequestToAttempt: CoverRequest?{
        if lastSuccessfullRequest == nil {
            return CoverRequest()
        }
        else{
            return lastSuccessfullRequest!.requestForNewer
        }
    }
    
    var covers: Array<Cover>?{
        didSet {
            if(oldValue?.count>0 ?? 0){
                coverTableView.reloadData()
            }
        }
    }
    
    var restClient: DBRestClient?
    
    // MARK: Actions
    @IBAction func refresh(sender: UIRefreshControl?) {
        
        if let request = nextRequestToAttempt{
            request.fetchCoversWithCompletionHandler{
                (source:Array<Cover>?) -> Void in
                    self.covers = source;
                    self.coverTableView?.reloadData()
                    sender?.endRefreshing()
            }
        }
        else{
            sender?.endRefreshing()
        }
    }
    
    // MARK: Functions
    func refresh(){
        
        if let _ = refreshControl{
            refreshControl?.beginRefreshing()
        }
        return refresh(refreshControl)
    }
    
    func updateUIWith(source: Array<Cover>?) -> Void{
        self.covers = source;
    }
    
    func pickNewCover() -> Void{
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.allowsEditing = false            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func cacheImage(image: UIImage?, withKey key:String){
        self.imageCache[key] = image
    }
    
    //MARK: implement UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if let selectedPath = coverTableView?.indexPathForSelectedRow{
            
            let selectedCell = coverTableView?.cellForRowAtIndexPath(selectedPath)! as! CoverTableViewCell
            let model = covers?[selectedPath.row]
            if let _ = model?.id{
                let key = "\(model!.id!)"
                cacheImage(image, withKey:key)
                saveImageToDropbox(image, withName: key)
            }
            selectedCell.img = image
        }
    }
    
    func restClient(client: DBRestClient!, uploadedFile destPath: String!, from srcPath: String!, metadata: DBMetadata!) {
        NSLog("File uploaded")
    }
    
    func restClient(client: DBRestClient!, uploadFileFailedWithError error: NSError!) {
        NSLog("File uploaded with error", error.description)
    }
    
    func saveImageToDropbox(image: UIImage, withName name:String){
        
        //save image to a path first
        let fileName = name.stringByAppendingString(CoverTableViewController.PNGExtension)
        let path = /*localPath*/getTempPath()!.NS.stringByAppendingPathComponent(fileName)
        let imageData = UIImagePNGRepresentation(image)
        if let _=imageData{
            do {
                try imageData!.writeToFile(path, options: NSDataWritingOptions.AtomicWrite)
                //save to dropbox
                self.restClient?.uploadFile(fileName, toPath: CoverTableViewController.DropboxPath, withParentRev: nil, fromPath: path)
            }
            catch let error {
                debugPrint("save file error: \(error)")
                return
            }
        }
    }
    
    func loadDropbox(){
        self.restClient!.loadMetadata(CoverTableViewController.DropboxPath)
    }
    
    func getTempPath() -> String?{
        //return NSTemporaryDirectory()
        let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        return documentsDirectoryPath
        
    }
    
    //MARK: Dropbox delegate
    func restClient(client: DBRestClient!, loadedFile destPath: String!, contentType: String!, metadata: DBMetadata!) {
        NSLog("File loaded into path:\(destPath)");
        
        var key = metadata.path
        key = key.stringByReplacingOccurrencesOfString("/", withString: "").stringByReplacingOccurrencesOfString(CoverTableViewController.PNGExtension, withString: "")
        
        let readPath = destPath.stringByAppendingString(metadata.path)
        do{
            let imgData = try NSData(contentsOfFile: readPath, options: NSDataReadingOptions.DataReadingUncached)
            let img = UIImage(data: imgData) //UIImage(contentsOfFile:readPath)
            if let _ = img{
                self.imageCache[key] = img
                self.coverTableView.reloadData() //should look for the row and reload it only instead
            }
        }
        catch let error {
            debugPrint("\(error)")
        }        
    }
    
    func restClient(client: DBRestClient!, loadFileFailedWithError error: NSError!) {
            NSLog("There was an error loading the file", error);
    }
    
    func restClient(client: DBRestClient!, loadMetadataFailedWithError error: NSError!) {
        NSLog("Error loading metadata: " + error.description);
        refresh()
    }
    
    func restClient(client: DBRestClient!, loadedMetadata metadata: DBMetadata!) {
        
        let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        
        if metadata.isDirectory{
            for file in metadata.contents!{
                    self.restClient?.loadFile(file.path, intoPath: documentsDirectoryPath as String)
                }
            }
        
        refresh()
    }
    
    func initRestClient()-> Void{
        restClient = DBRestClient.init(session: DBSession.sharedSession())
        restClient!.delegate = self
    }
    
    // MARK: View Controller lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !DBSession.sharedSession().isLinked()
        {
            //If not linked then start linking here..
            DBSession.sharedSession().linkFromController(self)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoverTableViewController.handleDidLinkNotification(_:)), name:Constants.linkToDropboxNotification, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoverTableViewController.handleCancelLinkNotification(_:)), name:Constants.cancelLinkDropboxNotification, object: nil)
        }
        else{
            initRestClient()
            loadDropbox()
        }
    }
    
    func handleDidLinkNotification(notification: NSNotification) {
        initRestClient()
        loadDropbox()
    }
    
    func handleCancelLinkNotification(notification: NSNotification) {
        refresh()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return covers?.count ?? 0;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DefaultCell", forIndexPath: indexPath) as! CoverTableViewCell
        
        if let _ = covers{
            let model = covers![indexPath.row]
            
            //if the image was previously cached, will be served from cache, else will be fetched
            if let _ = model.id, cachedImage: UIImage = imageCache["\(model.id!)"]{
                cell.img = cachedImage
            }
            else{
                cell.cover = model // to be downloaded
                if let _ = model.id{
                    let key = "\(model.id!)"
                    cell.actionAfterDownload = {[unowned self] (image: UIImage?) -> Void in self.cacheImage(image, withKey:key)}
                }
            }
        }
        

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        pickNewCover()
    }

}
